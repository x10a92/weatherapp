# WeatherApp

# WeatherApp

## Overview
WeatherApp is a SwiftUI-based iOS application that displays the current weather and forecasts using the OpenWeatherMap API. The app demonstrates the use of the MVVM architecture and includes features such as location tracking, weather fetching for the user's current location, and the ability to view weather in other cities.

## Features
- Request permission to track the user's location.
- Display current temperature and weather conditions.
- Provide weather-based advice.
- Allow users to change the location and view weather for other cities.
- Display a multi-day weather forecast.
- Cache weather data for offline viewing.

## Architecture
The application follows the MVVM (Model-View-ViewModel) architecture:
- **Model:** Represents weather data and API responses.
- **View:** SwiftUI views for displaying weather information.
- **ViewModel:** Manages the state and business logic.

## Installation
To run the project, follow these steps:
1. Clone the repository.
2. Open the project in Xcode.
3. Replace `YOUR_API_KEY` in `WeatherService.swift` with your OpenWeatherMap API key.
4. Build and run the project on your simulator or device.

## Issues and Considerations
- Ensure location permissions are granted to fetch the current location.
- Handle potential errors when fetching data from the API.
- The app currently uses `UserDefaults` for caching; consider using `CoreData` for more complex data storage.

## Time Spent
- Project Setup and Initial Configuration: 2 hours
- Model and Service Layer: 5 hours
- ViewModel Layer: 4 hours
- View Layer: 9 hours
- Location Handling: 2 hours
- Custom Tab Bar: 3 hours
- Offline Storage: 2 hours
- Error Handling and Testing: 1 hours
- inal Documentation and Code Review: 1 hours

