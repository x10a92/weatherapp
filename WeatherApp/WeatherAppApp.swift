//
//  WeatherAppApp.swift
//  WeatherApp
//
//  Created by Nikolay Fokin on 26.05.2024.
//

import SwiftUI

@main
struct WeatherAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
