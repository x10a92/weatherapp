//
//  ForecastView.swift
//  WeatherApp
//
//  Created by Nikolay Fokin on 26.05.2024.
//

import SwiftUI

struct ForecastView: View {
    @StateObject private var viewModel = WeatherViewModel()
    
    var body: some View {
        NavigationView {
            List(viewModel.forecast, id: \.date) { weather in
                VStack(alignment: .leading) {
                    Text("\(weather.date, formatter: DateFormatter.defaultDateFormatter)")
                    Text("Temperature: \(weather.temperature, specifier: "%.1f")°C")
                    Text(weather.description)
                    
                    let icon = weather.icon
                    let iconUrl = URL(string: "https://openweathermap.org/img/wn/\(icon)@2x.png")
                    AsyncImage(url: iconUrl)
                }
            }
            .navigationTitle("Forecast")
        }
        .onAppear {
            viewModel.fetchForecast()
        }
    }
}

extension DateFormatter {
    static let defaultDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .medium
        formatter.locale = Locale.current
        return formatter
    }()
}

struct ForecastView_Previews: PreviewProvider {
    static var previews: some View {
        ForecastView()
    }
}
