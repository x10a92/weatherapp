//
//  CustomTabBar.swift
//  WeatherApp
//
//  Created by User on 27.05.2024.
//

import SwiftUI

struct CustomTabBar: View {
    
    @State private var selectedTab = 0
    
    var body: some View {
        VStack {
            Spacer()
            // Content based on the selected tab
         
                Group {
                    switch selectedTab {
                    case 0:
                        MainView()
                    case 1:
                        AddButtonView()
                    case 2:
                        ForecastView()
                    default:
                        MainView()
                    }
                }.padding(.bottom, -40)
            
            
            Spacer()
            // Custom Tab Bar
            ZStack { Rectangle()
                    .frame(height: 80)
                    .padding(.bottom, -50)
                    .foregroundColor(.white)
                    .shadow(color: Color(red: 0, green: 0, blue: 0).opacity(0.1), radius: 20, x: 0, y: -5)
                    .opacity(1)
                HStack {
                    Spacer()
                    Button(action: {
                        selectedTab = 0
                    }) {
                        VStack {
                            Image(systemName: "thermometer")
                                .font(.system(size: 34))
                            Text("Weather")
                                .font(.caption)
                        }
                    }
                    .foregroundColor(selectedTab == 0 ? .purple : .gray)
                    Spacer()
                    
                    Spacer().frame(width: 100)
                   
                    Spacer()
                    Button(action: {
                        selectedTab = 2
                    }) {
                        VStack {
                            Image(systemName: "calendar")
                                .font(.system(size: 34))
                            Text("Forecast")
                                .font(.caption)
                        }
                    }
                    .foregroundColor(selectedTab == 2 ? .purple : .gray)
                    Spacer()
                }.padding(.bottom, -50)
                Button(action: {
                    print("Center button tapped")
                }) {
                    ZStack {
                        Circle()
                            .foregroundColor(Color(red: 0.4, green: 0.3, blue: 0.8))
                            .frame(width: 95, height: 95)
                        Image(systemName: "plus")
                            .foregroundColor(.white)
                            .font(.system(size: 28))
                    }
                    .shadow(
                        color: Color.black.opacity(0.4),
                        radius: 10,
                            x: 0,
                            y: 10)
                }
                .offset(y: 5) // Move the button upwards
                
               
            }
            .padding(.bottom, 20) // Add some padding at the bottom
        }
    }
}

    /*
struct CustomTabBar: View {
    
    var body: some View {
        TabView {
            MainView()
                .tabItem {
                    Image(systemName: "thermometer")
                    Text("Main")
                }
            
            AddButtonView()
                .tabItem {
                    Image(systemName: "plus.circle.fill")
                        .resizable()
                        .frame(width: 200, height: 200)
                }
            
            ForecastView()
                .tabItem {
                    Image(systemName: "calendar")
                    Text("Forecast")
                }
        }
    }
}
     */
 

struct AddButtonView: View {
    var body: some View {
        Text("Add Button")
            .font(.largeTitle)
    }
}

struct CustomTabBar_Previews: PreviewProvider {
    static var previews: some View {
        CustomTabBar()
    }
}
