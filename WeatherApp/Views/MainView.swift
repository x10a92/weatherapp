//
//  MainView.swift
//  WeatherApp
//
//  Created by Nikolay Fokin on 26.05.2024.
//

import SwiftUI

struct MainView: View {
    @StateObject private var viewModel = WeatherViewModel()
    @State private var cityName: String = ""
    
    @State private var showAlert = false
    @State private var fetchingError: Error?
    
    var body: some View {
        VStack {
            if viewModel.fetchingWeather {
                ProgressView("Fetching weather...")
            } else {
                if let weather = viewModel.weather {
                    Text("Current Temperature: \(weather.temperature, specifier: "%.1f")°C")
                    Text(weather.description)
                    
                    let icon = weather.icon
                    let iconUrl = URL(string: "https://openweathermap.org/img/wn/\(icon)@2x.png")
                    AsyncImage(url: iconUrl)
                    
                    Text(developerAdvice(for: weather.temperature))
                } else {
                    Text("No weather data available.")
                }
                
                TextField("Enter city name", text: $cityName)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(10)
                    .padding()
                
                Button(action: {
                    viewModel.fetchWeather(for: cityName) {  result in processWeatherResult(result) }
                }) {
                    Text("Get Weather")
                        .padding()
                        .background(Color.blue)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                }
                .padding()
                .alert(
                    Text("Something goes wrong"),
                    isPresented: $showAlert
                ) {
                    
                }
            }
        }
        .onAppear {
            viewModel.fetchWeather() {  result in processWeatherResult(result) }
        }
        /*
        .onReceive(viewModel.$weather) { _ in
            isFetching = false
        }
*/    }
    
    private func processWeatherResult(_ result: Result<WeatherResponse, Error>) {
        switch result {
        case .failure(let error):
            fetchingError = error
            showAlert = true
            break
        default:
            break
        }
    }
    
    private func developerAdvice(for temperature: Double) -> String {
        switch temperature {
        case ..<0:
            return "It's freezing! Stay warm."
        case 0..<15:
            return "It's a bit chilly. Dress warmly."
        default:
            return "It's warm outside. Enjoy the weather!"
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
