//
//  ContentView.swift
//  WeatherApp
//
//  Created by Nikolay Fokin on 26.05.2024.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var locationManager = LocationManager.shared
    
    var body: some View {
        if locationManager.userLocation != nil {
            CustomTabBar()
            /*
            TabView {
                MainView()
                    .tabItem {
                        Image(systemName: "thermometer")
                        Text("Main")
                    }
                
                ForecastView()
                    .tabItem {
                        Image(systemName: "calendar")
                        Text("Forecast")
                    }
            }
             */
        } else {
            LocationRequestView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct LocationRequestView: View {
    var body: some View {
        return VStack {
            Text("This app requires your device location for providing app services")
                .multilineTextAlignment(.center)

            Spacer()
                .frame(height: 100)
            
            Button("Allow Location Request") {
                LocationManager.shared.requestLocation()
            }
        }
    }
}
