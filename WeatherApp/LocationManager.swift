//
//  LocationManager.swift
//  WeatherApp
//
//  Created by NIKOLAY FOKIN on 30.05.2024.
//
 
import CoreLocation
import SwiftUI

class LocationManager: NSObject, ObservableObject  {
    private let manager = CLLocationManager()
    @Published var userLocation: CLLocation?
    
    static let shared = LocationManager()
    
    override init() {
        super.init()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.startUpdatingLocation()
    }
    
    func requestLocation() {
        manager.requestWhenInUseAuthorization()
    }
    
    func getCityLocation(for city: String, completion: @escaping (Result<CLLocation, Error>) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(city) { (placemarks, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let placemark = placemarks?.first, let location = placemark.location else {
                completion(.failure(NSError(domain: "No location found", code: 1, userInfo: nil)))
                return
            }
            
            completion(.success(location))
        }
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let first = locations.first else { return }        
        userLocation = first
    }
}
