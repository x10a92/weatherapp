//
//  CityModel.swift
//  WeatherApp
//
//  Created by NIKOLAY FOKIN on 31.05.2024.
//

import Foundation

struct CityModel: Codable {
    let coordinate: Coord
    let name: String
}
