//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by Nikolay Fokin on 26.05.2024.
//

import Foundation

struct Weather: Codable {
    let temperature: Double
    let description: String
    let icon: String
    let date: Date
    
    static func create(for data: ForecastWeatherData) -> Weather? {
        guard let firstWeather = data.weather.first else { return nil }
        return Weather(
            temperature: data.main.temp,
            description: firstWeather.description,
            icon: firstWeather.icon,
            date: Date(timeIntervalSince1970: Double(data.dt))
            )
    }
}

struct DailyWeatherData: Codable {
    let dt: TimeInterval
    let temp: Temperature
    let weather: [WeatherCondition]
}

struct Temperature: Codable {
    let day: Double
}

struct WeatherCondition: Codable {
    let description: String
    let icon: String
}

// MARK: - WeatherResponse
struct WeatherResponse: Codable {
    let coord: Coord
    let weather: [WeatherElement]
    let base: String
    let main: Main
    let visibility: Int
    let wind: Wind
    let clouds: Clouds
    let dt: Int
    let sys: Sys
    let timezone: Int
    let id: Int
    let name: String
    let cod: Int
}

// MARK: - WeatherData
struct WeatherData: Codable {
    let dt: Int
    let main: Main
    let weather: [WeatherElement]
    let clouds: Clouds
    let wind: Wind
    let visibility: Int
    let pop: Double
    let rain: Rain?
    let sys: Sys
    let dtTxt: String

    enum CodingKeys: String, CodingKey {
        case dt, main, weather, clouds, wind, visibility, pop, rain, sys
        case dtTxt = "dt_txt"
    }
}

// MARK: - WeatherElement
struct WeatherElement: Codable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

// MARK: - ForecastResponse
struct ForecastResponse: Codable {
    let cod: String
    let message: Int
    let cnt: Int
    let list: [ForecastWeatherData]
}

// MARK: - WeatherData
struct ForecastWeatherData: Codable {
    let dt: Int
    let main: Main
    let weather: [ForecastWeatherElement]
    let clouds: Clouds
    let wind: Wind
    let visibility: Int
    let pop: Double
    let rain: Rain?
    let sys: Sys
    let dtTxt: String

    enum CodingKeys: String, CodingKey {
        case dt, main, weather, clouds, wind, visibility, pop, rain, sys
        case dtTxt = "dt_txt"
    }
}

// MARK: - Clouds
struct Clouds: Codable {
    let all: Int
}

// MARK: - Coord
struct Coord: Codable {
    let lon: Double
    let lat: Double
}

// MARK: - Main
struct Main: Codable {
    let temp: Double
    let feelsLike: Double
    let tempMin: Double
    let tempMax: Double
    let pressure: Int
    let humidity: Int
    let seaLevel: Int?
    let grndLevel: Int?

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure, humidity
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
    }
}

// MARK: - Sys
struct Sys: Codable {
    let country: String?
    let sunrise: Int?
    let sunset: Int?
    let pod: String?
}

// MARK: - ForecastWeatherElement
struct ForecastWeatherElement: Codable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

// MARK: - Wind
struct Wind: Codable {
    let speed: Double
    let deg: Int
    let gust: Double?
}

// MARK: - Rain
struct Rain: Codable {
    let threeH: Double?

    enum CodingKeys: String, CodingKey {
        case threeH = "3h"
    }
}
