//
//  WeatherViewModel.swift
//  WeatherApp
//
//  Created by Nikolay Fokin on 26.05.2024.
//

import Foundation
import Combine
import CoreLocation

class WeatherViewModel: NSObject, ObservableObject {
    @Published var fetchingWeather: Bool = false;
    @Published var city: CityModel?
    
    @Published var weather: Weather?
    @Published var forecast: [Weather] = []
    
    private var weatherService = WeatherService()
    
    static let weatherDataKey = "weatherData"
    static let forecastDataKey = "forecastData"
    static let cacheDateKey = "cacheDate"
    
    override init() {
        super.init()
         
        // Load cached weather data
        loadCachedWeather()
    }
    
    func getLocation() -> CLLocation? {
        if let city = city {
            let coordinate = city.coordinate
            return CLLocation(latitude: coordinate.lat, longitude: coordinate.lon)
        }
        
        return LocationManager.shared.userLocation
    }
    
    func fetchWeather(for location: CLLocation, completion: @escaping (Result<WeatherResponse, Error>) -> Void) {
        self.fetchingWeather = true
        weatherService.fetchWeather(for: location) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let weatherResponse):
                    let firstWeather = weatherResponse.weather.first
                    self?.weather = Weather(temperature: weatherResponse.main.temp,
                                            description: firstWeather?.description ?? "",
                                            icon: firstWeather?.icon ?? "",
                                            date: Date())
                    
                    // Cache the weather data
                    guard let self = self else { return }
                    self.cacheWeatherData()
                    self.fetchingWeather = false
                    
                case .failure(let error):
                    guard let self = self else { return }
                    print("Error fetching weather: \(error)")
                    self.loadCachedWeather()
                    self.fetchingWeather = false
                }
            }
        }
    }
      
    func fetchWeather(completion: @escaping (Result<WeatherResponse, Error>) -> Void) {
        guard let location = getLocation() else {
            return
        }
        
        fetchWeather(for: location, completion: completion)
    }
    
    func fetchWeather(for city: String, completion: @escaping (Result<WeatherResponse, Error>) -> Void) {
        LocationManager.shared.getCityLocation(for: city) { [weak self] result in
            switch result {
            case .success(let location):
                if let self = self {
                    let locationCoordinate = location.coordinate
                    let coordinate = Coord(lon: locationCoordinate.longitude, lat: locationCoordinate.latitude)
                    self.city = CityModel(coordinate: coordinate, name: city)
                    
                    fetchWeather(for: location, completion: completion)
                }
            case .failure(let error):
                completion(.failure(NSError(domain: "Invalid City", code: 1, userInfo: nil)))
                print("Error fetching city: \(error)")
            }
        }
    } 
    
    func fetchForecast() {
        guard let location = getLocation() else {
            return
        }
        
        fetchForecast(for: location)
    }
    
    func fetchForecast(for location: CLLocation) {
        weatherService.fetchForecast(for: location) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let forecastResponse):
                    
                    self?.forecast = forecastResponse.list.compactMap {
                        return Weather.create(for: $0)
                    }                    
                    
                    // Cache the weather data
                    self?.cacheWeatherData()
                    
                case .failure(let error):
                    print("Error fetching weather: \(error)")
                    self?.loadCachedWeather()
                }
            }
        }
    }
    
    private func cacheWeatherData() {
        let encoder = JSONEncoder()
        if let weatherData = try? encoder.encode(weather),
           let forecastData = try? encoder.encode(forecast) {
            UserDefaults.standard.set(weatherData, forKey: WeatherViewModel.weatherDataKey)
            UserDefaults.standard.set(forecastData, forKey: WeatherViewModel.forecastDataKey)
            UserDefaults.standard.set(Date(), forKey: WeatherViewModel.cacheDateKey)
        }
    }
    
    private func loadCachedWeather() {
        let decoder = JSONDecoder()
        guard let cacheDate = UserDefaults.standard.object(forKey: WeatherViewModel.cacheDateKey) as? Date,
              Date().timeIntervalSince(cacheDate) < 3600 else {
            return // Cached data is older than 1 hour, discard it
        }
        
        if let weatherData = UserDefaults.standard.data(forKey: WeatherViewModel.weatherDataKey),
           let cachedWeather = try? decoder.decode(Weather.self, from: weatherData) {
            self.weather = cachedWeather
        }
        if let forecastData = UserDefaults.standard.data(forKey: WeatherViewModel.forecastDataKey),
           let cachedForecast = try? decoder.decode([Weather].self, from: forecastData) {
            self.forecast = cachedForecast
        }
    }
}
